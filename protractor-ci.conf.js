var VideoReporter = require('protractor-video-reporter');

const HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');
const jasmineReporters = require('jasmine-reporters');

const reportsDirectory = './_reports';
const dashboardReportDirectory = reportsDirectory + '/dashboardReport';
const detailsReportDirectory = reportsDirectory + '/detailReport';
const pathForVideoStorage = reportsDirectory + '/videos';

const config = require('./protractor.conf').config;
const { SpecReporter } = require('jasmine-spec-reporter');

var selectedRegion = process.env.region; //choose between NA, ANZ, EMEA
var selectedEnvironment = process.env.environment; //choose between dev, test, stage, prod, local
var myLogin = ""; //choose between Seth, Jose, Fabian, Craig, Micah, or leave "" if you want it normal
var baseUrl;
var region;
var login;
var password;
var apiGateway;
var baseApi;
let auth0;
let addPageAddition = true;
let multiUserUrl;
let auth0LogoutUrl;
let clientId;
let multiUserUrlArray;
switch(selectedRegion)
{
  case "NA":
    region = ".com";
    multiUserUrlArray = {
      "dev": 'http://MultiUserTest-08e3f61cc45501e5.elb.us-east-1.amazonaws.com',
      "test": 'http://MultiUserTest-08e3f61cc45501e5.elb.us-east-1.amazonaws.com',
      "stage": 'http://MultiUserStaging-44643587fb3a9bae.elb.us-east-1.amazonaws.com',
      "prod":	'http://MultiUser-f1c0f63ac985a4b5.elb.us-east-1.amazonaws.com'
    };
    break;
  case "ANZ":
    region = "s.com.au";
    multiUserUrlArray = {
      "dev": 'http://MultiUserTest-08e3f61cc45501e5.elb.us-east-1.amazonaws.com',
      "test": 'http://MultiUserTest-16fffb75461a6dca.elb.ap-southeast-2.amazonaws.com',
      "stage": 'http://MultiUserStaging-2a837668efea5dbf.elb.ap-southeast-2.amazonaws.com',
      "prod":	'http://MultiUser-3f073c007688acc5.elb.ap-southeast-2.amazonaws.com'
    };
    break;
  case "EMEA":
    region = ".eu";
    multiUserUrlArray = {
      "dev": 'http://MultiUserTest-365bc0c99e1690e7.elb.eu-central-1.amazonaws.com',
      "test": 'http://MultiUserTest-365bc0c99e1690e7.elb.eu-central-1.amazonaws.com',
      "stage": 'http://MultiUserStaging-3d6d18919401102f.elb.eu-central-1.amazonaws.com',
      "prod":	'http://MultiUser-387f7fd11fe5e251.elb.eu-central-1.amazonaws.com'
    };
    break;
}
switch(selectedEnvironment) {
  case "dev":
    baseUrl = "https://onekeydev.milwaukeetool" + region;
    login = 'devAutomation@mailinator.com';
    password = 'Password1';
    apiGateway = 'https://onekeyapigw.milwaukeetool.com/dev/';
    baseApi = 'https://onekeyapidev.milwaukeetool.com';
    auth0 = 'https://idtest.milwaukeetool.com/';
    multiUserUrl = 'http://MultiUserTest-08e3f61cc45501e5.elb.us-east-1.amazonaws.com';
    auth0LogoutUrl = 'https://idtest.milwaukeetool.com/';
    clientId = 'K0ZQDsDJjmsgkIDOrd0p72ynaFXEg6SY';
    break;
  case "test":
    baseUrl = "https://onekeytest.milwaukeetool" + region;
    login = 'testAutomation@mailinator.com';
    password = 'Password1';
    apiGateway = 'https://onekeyapigw.milwaukeetool.com/test/';
    baseApi = 'https://onekeyapitest.milwaukeetool.com';
    auth0 = 'https://idtest.milwaukeetool.com/';
    multiUserUrl = 'http://MultiUserTest-08e3f61cc45501e5.elb.us-east-1.amazonaws.com';
    auth0LogoutUrl = 'https://idtest.milwaukeetool.com/';
    clientId = 'K0ZQDsDJjmsgkIDOrd0p72ynaFXEg6SY';
    break;
  case "stage":
    login = 'stageAutomation@mailinator.com';
    password = 'Password1';
    apiGateway = 'https://onekeyapigw.milwaukeetool' + region + '/staging/';
    baseApi = 'https://onekeyapistaging.milwaukeetool' + region;
    auth0 = 'https://id.milwaukeetool' + region + '/';
    multiUserUrl = multiUserUrlArray.stage;
    auth0LogoutUrl = 'https://id.milwaukeetool' + region + '/';
    clientId = '0e329ri9YMvNojpLATvtuZ2Vsi8H7625';

    if(selectedRegion === "NA")
    {
      region = region + "/app";
    }
    baseUrl = "https://onekeystaging.milwaukeetool" + region;
    break;
  case "prod":
    baseUrl = "https://onekey.milwaukeetool" + region;
    login = 'prodAutomation@mailinator.com';
    password = 'Password1';
    apiGateway = 'https://onekeyapigw.milwaukeetool.com/prod/';
    baseApi = 'https://onekeyapiprod.milwaukeetool.com';
    auth0 = 'https://id.milwaukeetool.com/';
    multiUserUrl = 'http://MultiUser-f1c0f63ac985a4b5.elb.us-east-1.amazonaws.com';
    auth0LogoutUrl = 'https://id.milwaukeetool.com/';
    clientId = '0e329ri9YMvNojpLATvtuZ2Vsi8H7625';
    break;
  case "local":
    baseUrl = 'http://localhost:4200';
    login = 'devautomation@mailinator.com';
    password = 'Password1';
    apiGateway = 'https://onekeyapigw.milwaukeetool.com/dev/';
    baseApi = 'https://onekeyapidev.milwaukeetool.com';
    auth0 = 'https://idtest.milwaukeetool.com/';
    multiUserUrl = 'http://MultiUserTest-08e3f61cc45501e5.elb.us-east-1.amazonaws.com';
    auth0LogoutUrl = 'https://idtest.milwaukeetool.com/';
    clientId = 'K0ZQDsDJjmsgkIDOrd0p72ynaFXEg6SY';
    break;
}

if(myLogin != ""){
  login = myLogin + login;
  addPageAddition = false;
}

const ScreenshotAndStackReporter = new HtmlScreenshotReporter({
  dest: detailsReportDirectory,
  filename: 'E2ETestingReport.html',
  reportTitle: "E2E Testing Report",
  showSummary: true,
  reportOnlyFailedSpecs: false,
  captureOnlyFailedSpecs: false,
});

config.params = {
  ApiGateway: apiGateway,
  BaseApi:baseApi,
  Auth0: auth0,
  password: password,
  email: login,
  baseUrl: baseUrl,
  selectedEnvironment: selectedEnvironment,
  AddPageAddition: addPageAddition,
  MultiUserUrl: multiUserUrl,
  Auth0LogoutUrl: auth0LogoutUrl,
  ClientId: clientId
};

config.specs = process.env.specs || './**/*.e2e-spec.ts';

config.defaultTimeoutInterval = 15 * 60 * 1000; //15 minutes
config.allScriptsTimeout = 10 * 60 * 1000; //10 minutes

const loginInfo = process.env.login;
config.login = loginInfo.split('/');
config.baseUrl = process.env.baseUrl || 'https://onekeytest.milwaukeetool.com';
config.userName = config.login[0]  || 'stageAutomation@mailinator.com';
config.password = config.login[1]  || '1Keyautomation';
console.log("BaseUrl: " + process.env.baseUrl);
console.log("Specs: " + process.env.specs);
console.log("Username: " + config.userName);
console.log("password: " + config.password);
config.capabilities.chromeOptions.args = ['--no-sandbox', "--disable-gpu", "--window-size=1920x1080", '--disable-dev-shm-usage'];
config.restartBrowserBetweenTests = false;
config.beforeLaunch = function () {
  return new Promise(function (resolve) {
    ScreenshotAndStackReporter.beforeLaunch(resolve);
  });
};
config.onPrepare = async() => {

  require('ts-node').register({
    project: require('path').join(__dirname, './tsconfig.e2e.json')
  });
  jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
    consolidateAll: true,
    savePath: reportsDirectory + '/xml',
    SELENIUM_PROMISE_MANAGER: false,
    filePrefix: 'xmlOutput'
  }));
  jasmine.getEnv().addReporter(ScreenshotAndStackReporter);

  const fs = require('fs-extra');
  if (!fs.existsSync(pathForVideoStorage)) {
    fs.mkdirSync(pathForVideoStorage);
  }

  VideoReporter.prototype.jasmineStarted = function() {
    var self = this;
    if (self.options.singleVideo) {
      var videoPath = require('path').join(self.options.baseDirectory, 'protractor-specs.avi');

      self._startScreencast(videoPath);

      if (self.options.createSubtitles) {
        self._subtitles = [];
        self._jasmineStartTime = new Date();
      }
    }
  };

  jasmine.getEnv().addReporter(new VideoReporter({
    baseDirectory: pathForVideoStorage,
    singleVideoPath: 'fullName',
    createSubtitles: true,
    singleVideo: false,
    saveSuccessVideos: process.env.saveSuccessVideos,
    ffmpegArgs: [
      '-y',
      '-f', 'x11grab',
      '-framerate', '24',
      '-video_size', '1920x1080',
      '-i', process.env.DISPLAY,
      '-q:v','10',
    ]
  }));


  //const fs = require('fs-extra');
  if (!fs.existsSync(reportsDirectory)) {
    fs.mkdirSync(reportsDirectory);
  }
  if (!fs.existsSync(dashboardReportDirectory)) {
    fs.mkdirSync(dashboardReportDirectory);
  }

  jasmine.getEnv().addReporter({
    specDone: function (result) {
      if (result.status === 'failed') {
        browser.getCapabilities().then(function (caps) {
          const browserName = caps.get('browserName');

          browser.takeScreenshot().then(function (png) {
            const stream = fs.createWriteStream(dashboardReportDirectory + '/' + browserName + '-' + result.fullName + '.png');
            stream.write(new Buffer(png, 'base64'));
            stream.end();
          });
        });
      }
    }
  });
  jasmine.getEnv().addReporter(new SpecReporter({
    spec: {
      displayStacktrace: true
    },
    displayStacktrace: 'all',      // display stacktrace for each failed assertion, values: (all|specs|summary|none)
    displaySuccessesSummary: true, // display summary of all successes after execution
    displayFailuresSummary: true,   // display summary of all failures after execution
    displayPendingSummary: true,    // display summary of all pending specs after execution
    displaySuccessfulSpec: true,    // display each successful spec
    displayFailedSpec: true,        // display each failed spec
    displayPendingSpec: false,      // display each pending spec
    displaySpecDuration: false,     // display each spec duration
    displaySuiteNumber: false,      // display each suite number (hierarchical)
    colors: {
      success: 'green',
      failure: 'red',
      pending: 'yellow'
    },
    prefixes: {
      success: '✓ ',
      failure: '✗ ',
      pending: '* '
    },
    customProcessors: []
  }));
};

config.onComplete= function () {
  var browserName, browserVersion;
  var capsPromise = browser.getCapabilities();

  capsPromise.then(function (caps) {
      browserName = caps.get('browserName');
      browserVersion = caps.get('version');
      platform = caps.get('platform');

      var HTMLReport = require('protractor-html-reporter-2');
      testConfig = {
          reportTitle: 'Protractor Test Execution Report',
          outputPath: dashboardReportDirectory,
          outputFilename: 'index',
          screenshotPath: './',
          testBrowser: browserName,
          browserVersion: browserVersion,
          modifiedSuiteName: false,
          screenshotsOnlyOnFailure: false,
          testPlatform: platform
      };
      new HTMLReport().from(reportsDirectory + '/xml/xmlOutput.xml', testConfig);
  });
}
exports.config = config;